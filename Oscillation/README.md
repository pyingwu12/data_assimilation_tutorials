# Oscillation

## 説明
減衰振動（Oscillation）を用いたデータ同化を理解するためのプログラム

## プログラムリスト
* Oscillation_KF.ipynb
* Oscillation_EnKF.ipynb
* Oscillation_4DVAR.ipynb
* Oscillatino_PF.ipynb
* Oscillation.py
* Visualization.py
* KalmanFilter.py
* GenerateEnsemble.py
* EnsembleKalmanFilter.py

## プログラムの詳細

### 教材
詳しくは各ノートブック参照

#### Oscillation_KF.ipynb
Kalman Filterによるデータ同化

#### Oscillation_EnKF.ipynb
Ensemble Kalman Filterによるデータ同化

#### Oscillation_4DVAR.ipynb
4次元変分法によるデータ同化

#### Oscillation_PF.ipynb
Particle Filterによるデータ同化


### ライブラリ
#### Oscillation.py
減衰項つき振動の時間積分と接線形モデルの計算をするライブラリ

##### 使い方
__Oscillationクラスをoscとしてインスタンス化する__

* 減衰振動モデルの変数
  * dt：時間積分のタイムステップ（default=0.01） 
  * k：ばね定数（default=0.5）
  * mass：質量（default=1.0）
  * dump：減衰定数（default=0.3）

* 減衰振動モデルの初期値
  * xi：初期位置
  * vi：初期速度
  * Pf:背景誤差共分散の初期値（default=None）
  
```
osc = Oscillation.Oscillation(xi,vi,noise,Pf,dt,k,mass,dump)
```

__dt秒後の位置と速度を得る__

* 戻り値の変数
  * x：dt秒後の位置
  * v：dt秒後の速度
  * xo：観測誤差を加味したdt秒後の位置
  * vo：観測誤差を加味したdt秒後の速度（観測誤差は明示的に0にしている）

```python
x, v = osc.time_integration()  
xo, vo = osc.observation_noise() # osc.time_integration()のあとに実行する
```

__背景誤差共分散を更新する__

* 戻り値の変数
  * Pf：更新された背景誤差共分散

```
Pf = osc.transient_matrix()
```

#### Visualization.py
データを描画し、RMSEを計算するライブラリ

##### 使い方
__Visualizationクラスをvisとしてインスタンス化する__

* データ同化に用いた変数
  * obs：観測値
  * true：真値
  * sim：モデルの推定値
  * da：データ同化による推定値
  * interval：データ同化の間隔ステップ数
  * nt\_asm：データ同化に用いた時間ステップ数

* グラフの設定に用いる変数
  * name：グラフのyラベル
  * xlim：グラフのx軸の範囲
  
```
vis = Visualization.Visualization(obs,true,sim,da,interval,nt_asm,name,xlim)
```

__グラフの描画とRMSEの計算__

まず、グラフの描画にはfitメソッドを用いる

```
vis.fit()
```

次に、データ同化による値と観測値とのRMSEの計算にはrmseメソッドを用いる

```
vis.rmse()
```

#### KalmanFilter.py
Kalman Filterを実施するライブラリ

##### 使い方
__KalmanFilterクラスをkfとしてインスタンス化する__

* データ同化に用いた変数
  * xv：ある時刻のモデルの位置と速度の推定値
  * noise：観測誤差
  * Pf：背景誤差共分散
  * H：観測誤差
  * xvobs：ある時刻の位置と速度の観測値（真値に観測誤差が入ったもの）
  
```
kf = KalmanFilter.KalmanFilter(xv,noise,Pf,H,xvobs)
```

__Kalman Filterを実行する__

* 戻り値の変数
  * xvs：データ同化した次の時刻の位置と速度の予測値
  * Pf：更新された背景誤差共分散

```
xvs, Pf = kf.fit()
```

#### GenerateEnsemble.py
初期のEnsembleを生成するライブラリ

##### 使い方
__GenerateEnsembleクラスをgeとしてインスタンス化する__

Ensembleは中心x、分散sの正規分布に従って生成される

* Ensemble生成に用いた変数
  * x：真値
  * n：生成するEnsemble数
  * s：Ensembleの分散
  
```
ge = GenerateEnsemble.GenerateEnsemble(x,n,s)
```

__Ensembleを生成する__

* 戻り値の変数
  * ens：生成されたEnsemble

```
ens = ge.fit()
```

#### EnsembleKalmanFilter.py
Ensemble Kalman Fileterを実施するライブラリ

##### 使い方
__EnsembleKalmanFilterクラスをekfとしてインスタンス化する__

* データ同化に用いた変数
  * ens：ある時刻のモデルの位置の推定値から生成したEnsemble
  * noise：観測誤差
  * H：観測誤差
  * xobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
ekf = KalmanFilter.KalmanFilter(ens,H,xobs,noise)
```

__Ensemble Kalman Filterを実行する__

Kalman Filterと異なり、背景誤差共分散行列をEnsembleから計算する

* 戻り値の変数
  * enss：データ同化した次の時刻の速度の予測値

```
enss = kf.fit()
```

#### ParticleFilter.py
粒子フィルタを実施するライブラリ

Ensembleメンバを用いて背景誤差共分散行列を計算する

##### 使い方
__ParticleFilterクラスをprfとしてインスタンス化する__

* データ同化に用いた変数
  * ens：ある時刻のモデルの位置の推定値から生成したEnsemble
  * noise：観測誤差
  * H：観測誤差
  * xobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
prf = KalmanFilter.KalmanFilter(ens,H,xobs,noise)
```

__Particle Filterを実行する__

* 戻り値の変数
  * enss：データ同化した次の時刻の速度の予測値

```
enss = kf.fit()
```
