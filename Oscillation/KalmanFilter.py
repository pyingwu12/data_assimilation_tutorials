import numpy as np

class KalmanFilter:
    def __init__(self,sim, noise,Pf, H, obs):
        self.sim = sim
        self.noise = noise
        self.Pf = Pf
        self.H = H
        self.obs = obs

    def fit(self):
        #Kalman gain
        _inv = np.dot(np.dot(self.H, self.Pf), self.H.T)
        for i in range(_inv.shape[0]):
            _inv[i,i] = _inv[i,i] + self.noise * self.noise
        _inv = np.linalg.inv(_inv)
        _Kg = np.dot(np.dot(self.Pf, self.H.T), _inv)

        #Innovation
        _innov = np.dot(self.H, self.obs) - np.dot(self.H, self.sim)

        #Update
        self.sim = self.sim + np.dot(_Kg, _innov)
        _Pa = self.Pf - np.dot(np.dot(_Kg, self.H), self.Pf)

        return self.sim, _Pa
